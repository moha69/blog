<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
class LoginsController extends Controller
{
   
    public function index()
    {
      return  view('Auth.login');
    }
    
    public function Logout()
    {
        
        Session::flush();
        Auth::logout();

      return  redirect('/');
    }
    /**
     */
    public function create()
    {
        //
    }

    /**
     */
    public function loginPost(Request $request)
    {
       
       $credetails=[
        'email'=>$request->email,
        'password'=>$request->password,
       ];
       if(Auth::attempt($credetails)){
        return redirect('/profile')->with('success','Login correct');
       }else{
        return back()->with('login','Email or Password not correct');

       }
    }

    /**
     */
    public function show(string $id)
    {
        //
    }

    /**
     */
    public function edit(string $id)
    {
        //
    }

    /**
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     */
    public function destroy(string $id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;

class PostsController extends Controller
{
   
    public function profile()
    {
        $posts = Post::where('user_id' , auth()->id())->get();

        return view('blog.profile' , compact('posts'));    }

        
    public function index()
    {
        return view('blog.index')->with('posts',Post::get()); 
    }

   
    public function create()
    {
        return view('blog.create');

       

    }

    
    public function store(Request $request)
{
    $request->validate([
        'title' => 'required',
        'description' => 'required',
        'image' => 'nullable|mimes:jpg,png,jpeg|max:5048'
    ]);

    $defaultImageName = 'image.png'; 

    if ($request->hasFile('image')) {
        $newImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();
        $request->image->move(public_path('image'), $newImageName);
    } else {
        $newImageName = $defaultImageName;
    }

    // Create a new post
    Post::create([
        'title' => $request->input('title'),
        'description' => $request->input('description'),
        'image_path' => $newImageName,
        'user_id' => auth()->user()->id
    ]);

    return redirect('/blog');
}

        

  
    public function show($id){
        
        $post = Post::find($id);

        return view('blog.show', compact('post'));
    }

   
  
    public function edit( $id)
    {
        $post = Post::find($id);

        return view('blog.edit', compact('post'));
    }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'nullable|mimes:jpg,png,jpeg|max:5048'
        ]);
    
        $post = Post::find($id);
    
        if ($request->hasFile('image')) {
            $newImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();
            $request->image->move(public_path('image'), $newImageName);
    
            if ($post->image_path !== 'no-image.png' && file_exists(public_path('image/' . $post->image_path))) {
                unlink(public_path('image/' . $post->image_path));
            }
    
            $post->image_path = $newImageName;
        }
    
        $post->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'image_path' => $post->image_path, 
            'user_id' => auth()->user()->id
        ]);
    
        return redirect('/profile')->with('message', 'Post is updated');
    }
    


    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();


        return redirect('/blog')->with('message', 'Post deleted successfully');
    }
}

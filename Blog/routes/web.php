<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\RegistersController;
use App\Http\Controllers\LoginsController;

Route::get('/', [HomeController::class, 'index']);

// Register routes:
Route::resource('/register', RegistersController::class);
Route::post('/register', [RegistersController::class, 'registerPost'])->name('register');


// Login routes:
Route::resource('/login', LoginsController::class);
Route::post('/login', [LoginsController::class, 'loginPost'])->name('login');
Route::get('/logout', [LoginsController::class, 'logout']);

// Public blog route:
Route::get('/blog/show/{id}', [PostsController::class, 'show']);
Route::get('/blog', [PostsController::class, 'index'])->name('blog.index');


// Authenticated routes:
Route::middleware(['auth'])->group(function () {
    // Blog routes:
    Route::get('/blog/create', [PostsController::class, 'create'])->name('blog.create');
    Route::post('/blog/store', [PostsController::class, 'store'])->name('blog.store');
    Route::get('/blog/edit/{id}', [PostsController::class, 'edit']);
    Route::put('/blog/update/{id}', [PostsController::class, 'update'])->name('blog.update');
    Route::delete('/blog/delete/{id}', [PostsController::class, 'destroy'])->name('blog.delete');

    // User profile route:
    Route::get('/profile', [PostsController::class, 'profile']);
});

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Public\image;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence, 
            'description' => $this->faker->text(200),
            'image_path' => $this->faker->image('public/image', 640, 480, null, false),
            'user_id' => rand(1, 10),
            'created_at' => $this->faker->dateTimeThisYear,
            'updated_at' => $this->faker->dateTimeThisYear,
        ];
    }
}

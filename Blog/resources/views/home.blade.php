@extends('layouts.navbar')
@section('content')
<style>
   .hero-bg-image{
    
    background:url('https://enviragallery.com/wp-content/uploads/2020/01/How-to-make-a-background-white.png') no-repeat center center/cover;
    height:500px;
}
</style>

<div class="hero-bg-image flex flex-col items-center justify-center ">
<h1 class="text-black-100 text-5xl uppercase font-bold pb-10">Welcome to my Blog</h1>
<a class="bg-gray-800 text-xl uppercase font-bold text-white rounded-lg py-4 px-5" href="/blog" >Start Reading</a>
</div>
@endsection

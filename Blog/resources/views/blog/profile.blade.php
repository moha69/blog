@extends('layouts.navbar')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MY BLOG</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

  <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
<h1 class=" flex  items-center justify-center text-6xl font-bold "> You Profile</h1>

@if (session()->has('success'))
    <div class="container mx-auto mt-4">
        <div id="alert" class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session()->get('success') }}</span>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3 cursor-pointer" onclick="document.getElementById('alert').style.display='none';">
                <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <title>Close</title>
                    <path d="M14.348 5.652a1 1 0 00-1.415-1.414L10 7.172 7.066 4.238a1 1 0 10-1.415 1.414L8.828 10l-3.177 3.177a1 1 0 001.415 1.414L10 12.828l2.934 2.934a1 1 0 001.415-1.414L11.172 10l3.176-3.177z"/>
                </svg>
            </span>
        </div>
    </div>
@endif
        @if (session()->has('message'))
    <div class="container mx-auto mt-4">
        <div id="alert" class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session()->get('message') }}</span>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3 cursor-pointer" onclick="document.getElementById('alert').style.display='none';">
                <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <title>Close</title>
                    <path d="M14.348 5.652a1 1 0 00-1.415-1.414L10 7.172 7.066 4.238a1 1 0 10-1.415 1.414L8.828 10l-3.177 3.177a1 1 0 001.415 1.414L10 12.828l2.934 2.934a1 1 0 001.415-1.414L11.172 10l3.176-3.177z"/>
                </svg>
            </span>
        </div>
    </div>
@endif
<section class="p-6 rounded-lg">
    <div class="flex items-center space-x-4">
    </div>
    
    <div class="mt-6">
        <h3 class="text-lg font-semibold">About Me</h3>
        <p class="text-gray-600 mt-2">
            Passionate developer with a knack for building applications with maximum efficiency.
            I have a keen interest in the latest web technologies.
        </p>
    </div>
    <div class="mt-6">
        <a href="/blog/create" class="bg-green-700 text-gray-100 py-3 px-3 rounded-lg font-bold uppercase text-l mb-3 hover:bg-green-900 ">
           + Create Post
        </a>
    </div>
    <div class="mt-8">
        <h3 class="text-lg font-semibold">Old Posts</h3>
        @foreach ($posts as $post)
        <div class="flex flex-col overflow-hidden bg-white rounded-lg shadow-md duration-300  hover:shadow-xl mt-4">
     
        <div class="px-6 py-4 flex">
        <div>
            <img src="/image/{{$post->image_path}}" alt="" width='150px'>
        </div>
        <div class='ms-5 mt-3'>
                <h2 class="text-2xl text-gray-700 font-bold">{{ $post->title }}</h2>
                <p class="mt-2 text-gray-600">{{ Str::limit($post->description) }}
                    <a href="/blog/show/{{$post->id}}" class="text-blue-500 hover:underline">Read more</a>
                </p>
                </div>
            </div><div class="px-6 py-4 border-t border-gray-200">
    <div class="flex justify-between items-center">
        <p class="text-gray-400 text-sm">Created at: {{ $post->created_at->format('Y-m-d') }}</p>
        <div class="flex space-x-2">
            <a href='/blog/edit/{{$post->id}}' class="bg-blue-500 text-gray-100 py-3 px-3 rounded-lg font-bold uppercase text-l mb-3 hover:bg-blue-900">
                edit post
            </a>
            <form action="{{ route('blog.delete', $post->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="bg-red-700 text-gray-100 py-3 px-3 rounded-lg font-bold uppercase text-l mb-3">
                    delete post
                </button>
            </form>
        </div>
    </div>
</div>

        </div>
        @endforeach
    </div>
</section>
</body>
</html>
@endsection
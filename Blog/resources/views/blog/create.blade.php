@extends('layouts.navbar')
@section('content') 
<h1 class="bg-gray-100 flex  items-center justify-center text-6xl font-bold   "> Add Post</h1>
<div class="bg-gray-100 min-h-screen flex items-center justify-center ">
    <div class="bg-white p-8 rounded-lg shadow-lg">
        <h2 class="text-2xl font-semibold text-gray-800 mb-4">Submit Content</h2>
        <form method="POST" action="/blog/store" enctype="multipart/form-data">
            @csrf
            <div class="mb-4">
                <label for="title" class="text-gray-700">Title</label>
                <input type="text" name="title" id="title" placeholder="Enter title"
                       class="block w-full border-gray-300 rounded-md focus:ring focus:ring-blue-400 focus:outline-none px-4 py-2 mt-2">
            </div>
            <div class="mb-4">
                <label for="description" class="text-gray-700">Description</label>
                <textarea name="description" id="description" rows="4" placeholder="Enter description"
                          class="block w-full border-gray-300 rounded-md focus:ring focus:ring-blue-400 focus:outline-none px-4 py-2 mt-2"></textarea>
            </div>
            <div class="mb-4">
                <label for="image" class="text-gray-700">Choose Image</label>
                <input type="file" name="image" id="image" accept="image/*"
                       class="block border-gray-300 rounded-md focus:ring focus:ring-blue-400 focus:outline-none px-4 py-2 mt-2">
            </div>
            <div class="mt-6">
                <button type="submit"
                        class="block w-full bg-blue-500 text-white font-semibold rounded-md py-2 hover:bg-blue-600 focus:outline-none focus:bg-blue-600">
                    Submit
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
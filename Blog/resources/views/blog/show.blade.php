@extends('layouts.navbar')
@section('content')

<div class="container mx-auto text-center pt-15 pb-5">
    <h2 class="text-6xl font-bold">{{ $post->title }}</h2>
    <div class="mt-2 text-gray-500 italic">
        By: <span>{{ $post->user->name }}</span> on <span>{{ $post->created_at->format('d-m-y') }}</span>
    </div>
</div>

<div class="container mx-auto pt-15 pb-5">
    <div class="flex flex-wrap justify-center">
            <div class="w-full md:w-1/2 lg:w-1/3 p-2">
                <img class="object-cover w-full h-96" src="/image/{{ $post->image_path }}" alt="{{ $post->title }}">
            </div>
        
    </div>
    <div class="text-lg text-gray-700 py-8 leading-6">
        {{ $post->description }}
    </div>
</div>
@endsection

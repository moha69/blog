@extends('layouts.navbar')
@section('content')
<div class="container m-auto text-center pt-15 pb-5 bg-5 ">
    <h1 class="text-6xl font-bold mt-10">All Posts</h1>
</div>

@if (session()->has('message'))
    <div class="container mx-auto mt-4">
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session()->get('message') }}</span>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                <svg class="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <title>Close</title>
                    <path d="M14.348 5.652a1 1 0 00-1.415-1.414L10 7.172 7.066 4.238a1 1 0 10-1.415 1.414L8.828 10l-3.177 3.177a1 1 0 001.415 1.414L10 12.828l2.934 2.934a1 1 0 001.415-1.414L11.172 10l3.176-3.177z"/>
                </svg>
            </span>
        </div>
    </div>
@endif

@foreach ($posts as $post)
<div class=" container sm:grid grid-cols-2 gap-10 mx-auto mt-12 py-12 px-5 border-b border-gray-500" >
    <div>
        <img src="/image/{{$post->image_path}}" alt="">
    </div>
    <div> 
        <div class="flex">
        <h2 class="text-gray-700 font-bold text-4xl py-5 md:pt-0">{{$post->title}}</h2>
        
    </div>
       
    By:<span class="text-gray-500 italic">{{$post->user->name}}</span>
    on<span class="text-gray-500 italic">{{ $post->created_at->format('d-m-y ') }}</span>
    <p class=" text-l text-gray-700 py-8 leading-6 ">{{$post->description}}</p>
    <div class='flex justify-between'>
    <a href='/blog/show/{{$post->id}}' class="bg-gray-700 text-gray-100 py-3 px-3 rounded-lg font-bold uppercase text-l mb-3">
        read more</a>
        @if (Auth::check() and Auth::user()->admin)
            <form action="{{ route('blog.delete', $post->id) }}" method="post">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="bg-red-700 text-gray-100 py-3 px-3 rounded-lg font-bold uppercase text-l mb-3">
                        delete post
                    </button>
                </form>
        
        @endif
        </div>

</div>
</div>
@endforeach
@endsection
@extends('layouts.navbar')
@section('content') 
<h1 class="bg-gray-100 flex  items-center justify-center text-6xl font-bold mt-2"> edit post</h1>
<div class="bg-gray-100 min-h-screen flex items-center justify-center">
   

    <div class="bg-white p-8 rounded-lg shadow-lg">
        <h2 class="text-2xl font-semibold text-gray-800 mb-4">Submit Content</h2>
        
        <form method="POST" action="{{ route('blog.update', $post->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mb-4">
                <label for="title" class="text-gray-700">Title</label>
                <input type="text" name="title" id="title" value='{{$post->title}}'
                       class="block w-full border-gray-300 rounded-md focus:ring focus:ring-blue-400 focus:outline-none px-4 py-2 mt-2">
            </div>
            <div class="mb-4">
                <label for="description" class="text-gray-700">Description</label>
                <textarea
                 name="description"
                 class="block w-full border-gray-300 rounded-md focus:ring focus:ring-blue-400 focus:outline-none px-4 py-2 mt-2">

                   {{$post->description}}
                </textarea>
            </div>
            <div class="flex">
                @if($post->image_path)
                <div>
                    <img src="{{ asset('image/' . $post->image_path) }}" alt="Post Image" width="100">
                </div>
            @endif
            <input type="file" name="image" id="image" class="form-control mt-6 ml-2">
            
        </div>
            <div class="mt-6">
                <button type="submit"
                        class="block w-full bg-blue-500 text-white font-semibold rounded-md py-2 hover:bg-blue-600 focus:outline-none focus:bg-blue-600">
                    Submit
                </button>
            </div>
        </form>
    </div>
</div>

<br><br>
@endsection
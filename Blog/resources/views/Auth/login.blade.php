
@extends('layouts.navbar')
@section('content')

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MY BLOG</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

  <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
@if (session()->has('success'))
    <div class="container mx-auto mt-4">
        <div id="alert" class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session()->get('success') }}</span>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3 cursor-pointer" onclick="document.getElementById('alert').style.display='none';">
                <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <title>Close</title>
                    <path d="M14.348 5.652a1 1 0 00-1.415-1.414L10 7.172 7.066 4.238a1 1 0 10-1.415 1.414L8.828 10l-3.177 3.177a1 1 0 001.415 1.414L10 12.828l2.934 2.934a1 1 0 001.415-1.414L11.172 10l3.176-3.177z"/>
                </svg>
            </span>
        </div>
    </div>
@endif


  <body class="bg-gray-100">
  <div class="min-h-screen flex flex-col justify-center  ">
    <div class="sm:mx-auto sm:w-full sm:max-w-md">
      <h2 class="mt-6 text-center text-3xl font-extrabold text-gray-900">Log in </h2>
      <div class="mt-8 bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
    <form class="space-y-6" action="{{route('login')}}" method="POST">
      @csrf
       
      <input type="hidden" name="remember" value="true">
      <div class="rounded-md shadow-sm -space-y-px mb-">
        <div>
        Email address:<br>
          <label for="email-address" class="sr-only">Email address</label>
          <input id="email-address" name="email" type="email" autocomplete="email" required class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Email address">
        @error("login")
          <div class='text-red '>{{$message}} </div>
        @enderror
       
        </div>
        <div>
         Password:
          <label for="password" class="sr-only mt-1">Password</label>
          <input id="password" name="password" type="password" autocomplete="current-password" required class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Password">
        </div>
      </div>

      <div class="flex items-center justify-between">
        <div class="flex items-center">
          <input id="remember_me" name="remember_me" type="checkbox" class="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded">
          <label for="remember_me" class="ml-2 block text-sm text-gray-900">
            Remember me
          </label>
        </div>

        <div class="text-sm">
          <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">
          </a>
        </div>
      </div>

      <div>
        <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
          <span class="absolute left-0 inset-y-0 flex items-center pl-3">
            <svg class="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
              <path fill-rule="evenodd" d="M10 12a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd"></path>
              <path fill-rule="evenodd" d="M4 8V6a6 6 0 1112 0v2h2a2 2 0 012 2v8a2 2 0 01-2 2H4a2 2 0 01-2-2v-8a2 2 0 012-2h2zm6-2a4 4 0 00-4 4v2h8V10a4 4 0 00-4-4z" clip-rule="evenodd"></path>
            </svg>
          </span>
          Log in
        </button>
      </div>
    </form>
    <div class="text-sm text-center">
      <p class="font-medium text-gray-900">
        Don't have an account? 
        <a href="/register" class="text-indigo-600 hover:text-indigo-500">
          Register
        </a>
      </p>
    </div>
  </div>
</div>



</body>
</html>
<br>
@endsection

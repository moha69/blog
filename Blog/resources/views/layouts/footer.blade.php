
    <div class="flex flex-col bg-gray-400  shadow-lg">
        <div class="w-full draggable">
            <div class="container flex flex-col mx-auto">
                <div class="flex flex-col items-center w-full my-20">
                    <h1 class="text-4xl  font-bold mb-6">MyBlog</h1>
                    <div class="flex flex-col items-center gap-6 mb-8">
                        <div class="flex flex-wrap items-center justify-center gap-5 lg:gap-12 gap-y-3 lg:flex-nowrap text-dark-grey-900">
                            <a href="/" class="text-white ">Home</a>
                            <a href="/blog" class="text-white ">Blog</a>
                            <a href="/login" class="text-white ">Login</a>
                            <a href="/register" class="text-white ">Register</a>
                        </div>
                    </div>
                    <span>
              <small> &copy; 2024 - MOHAMED </small>
            </span>
                </div>
            </div>
          
        </div>
    </div>
</html>




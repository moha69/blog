<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MY BLOG</title>
  <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body>
<nav class="bg-gray-400 py-4">
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 flex justify-between items-center">
        <div class="flex items-center">
            <a href="/" class="text-white text-lg font-semibold">MyBlog</a>
        </div>
        <div class="hidden md:flex items-center">
            <a href="/" class="ml-4 text-white hover:text-white">Home </a>
            <a href="/blog" class="ml-4 text-white hover:text-white">Blog</a>
            @auth
            <a href="/logout" class="ml-4 text-white hover:text-white">Logout</a>
            <a href="/profile" class="ml-4 text-yellow-300 hover:text-white"> {{auth()->user()->name}} </a>

            @endauth
            @guest
            <a href="/login" class="ml-4 text-white hover:text-white">Login</a>
            <a href="/register" class="ml-4 text-white hover:text-white">Register</a>
            @endguest
        </div>
    </div>
</nav>
@yield('content')
@include('layouts.footer')
</body>
</html>
